package com.example.restservice;

public class Greeting {

	private final String hostname;
	private final long id;
	private final String content;

	public Greeting(String hostname, long id, String content) {
		this.hostname = hostname;
		this.id = id;
		this.content = content;
	}

	public String getHostname() {
		return hostname;
	}

	public long getId() {
		return id;
	}

	public String getContent() {
		return content;
	}
}
