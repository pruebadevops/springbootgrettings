package com.example.restservice;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicLong;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

	private static final String template = "Hello , %s!";
	private final AtomicLong counter = new AtomicLong();

	@CrossOrigin(origins = "*", maxAge = 3600)
	@GetMapping("/greeting")
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
		String hostname = "";
		try {
			 hostname = InetAddress.getLocalHost().getHostName();	
		} catch ( UnknownHostException e) {
			//TODO: handle exception
		}
		
		return new Greeting(hostname,counter.incrementAndGet(), String.format(template, name));
	}
}
